function checkUserNameFormat(userInput, idErr) {
    let digitNumber = 0;
    for (let i = 0; i < userInput.length; i++) {
        let character = userInput.charAt(i)
        if (/^\d$/.test(character)) {
            digitNumber++
        }
    }

    if (digitNumber < 6 || digitNumber > 10) {
        showMessageErr(idErr, "userName must contain the 6-10 number of digit")
        return false
    }
    return true
}

function checkEmpty(userInput, idErr, message) {
    if (userInput.length == 0) {
        showMessageErr(idErr, message)
        return false
    } else {
        showMessageErr(idErr, "")
        return true
    }
}

function checkCharacter(userInput, idErr, message) {
    for (let i = 0; i < userInput.length; i++) {
        character = userInput.charAt(i)
        if (/^\d$/.test(character)) {
            showMessageErr(idErr, message)
            return false
        }
    }
    return true
}


function checkEmailFormat(userInput, idErr) {

    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(userInput)) {
        return (true)
    }
    showMessageErr(idErr, "invalid email address")
    return (false)

}


function checkPassword(userInput, idErr) {
    let charsNumber = 0;
    let digitNumber = 0
    let uppercaseNumber = 0
    let lowercaseNumber = 0
    let specialCharNumber = 0
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

    for (let i = 0; i < userInput.length; i++) {
        let character = userInput.charAt(i)
        if (specialChars.test(character)) {
            specialCharNumber++
        }
        else if (/^\d$/.test(character)) {
            digitNumber += 1
        }
        else if (character.match(/[a-z]/i)) {
            charsNumber++
            if (character == character.toUpperCase()) {
                uppercaseNumber++
            }
            else if (character == character.toLowerCase()) {
                lowercaseNumber++
            }
        }
    }


    if (charsNumber < 6 || charsNumber > 10) {
        showMessageErr(idErr, "The number of password's character is must be between 6 and 10")
        return false
    }
    if (digitNumber < 1) {
        showMessageErr(idErr, "password must contain at least 1 digit")
        return false
    }
    if (uppercaseNumber < 1) {
        showMessageErr(idErr, "password must contain at least 1 uppercase case character")
        return false
    }
    if (lowercaseNumber < 1) {
        showMessageErr(idErr, "password must contain at least 1 lower case character")
        return false
    }
    if (specialCharNumber < 1) {
        showMessageErr(idErr, "password must contain at least 1 special character")
        return false
    }
    return true
}


function checkSalary(userInput, idErr) {
    let salary = userInput * 1
    if (isNaN(salary)) {
        showMessageErr(idErr, "the salary must be in the number format")
        return false
    }
    else if (salary < 1000000 || salary > 20000000) {
        showMessageErr(idErr, "the salary must be between 1.000.000 VND and 20.000.000 VND")
        return false
    }
    return true
}

function checkTitle(userInput, idErr, message) {
    switch (userInput) {
        case "director":
            return true
        case "head":
            return true
        case "employee":
            return true
        default:
            showMessageErr(idErr, message)
            return false

    }

}

function checkWorkingHour(userInput, idErr) {
    let workingHour = userInput * 1

    if (isNaN(workingHour)) {
        showMessageErr(idErr, "the working hours must be numeric type")
        return false
    }
    else {
        if (workingHour < 80 || workingHour > 200) {
            showMessageErr(idErr, "the working hours must be between 80 hours and 200 hours in a month")
            return false
        }
        return true
    }
}


function checkDateFormat(userInput, idErr) {
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(userInput)) {
        showMessageErr(idErr, "invalid date")
        return false;

    }

    // Parse the date parts to integers
    var parts = userInput.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12) {
        showMessageErr(idErr, "invalid date")
        return false;
    }



    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}