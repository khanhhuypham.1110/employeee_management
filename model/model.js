
function makeId() {
    var result = ''
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (let i = 0; i < 5; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result
}

function Employee(username, fullName, email, password, startDate, basicSalary, title, workingHour) {
    this.id = makeId()
    this.username = username
    this.fullName = fullName
    this.email = email
    this.password = password
    this.startDate = startDate
    this.basicSalary = basicSalary
    this.title = title
    this.workingHour = workingHour

    this.setEmployeeId = function (id) {
        this.id = id
    }

    this.getEmployeeId = function () {
        return this.id
    }

    this.getGrossSalary = function () {
        if (this.title == "director") {
            return this.basicSalary * 3
        } else if (this.title == "head") {
            return this.basicSalary * 2
        } else if (this.title == "employee") {
            return this.basicSalary
        }
    }

    this.getEmpType = function () {
        if (this.workingHour >= 192) {
            return "excellent employee"
        }
        else if (this.workingHour >= 176) {
            return "very good employee"
        }
        else if (this.workingHour >= 160) {
            return "good employee"
        }
        else if (this.workingHour < 160) {
            return "ordinary employee"
        }
    }

}
