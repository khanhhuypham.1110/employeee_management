let employeeList = []
let dataJSON = localStorage.getItem("employeeList")
let parsedData = JSON.parse(dataJSON)
if (parsedData.length == 0) {

} else {
    for (let i = 0; i < parsedData.length; i++) {
        var item = parsedData[i]
        var employee = new Employee(item.username, item.fullName, item.email, item.password, item.startDate, item.basicSalary, item.title, item.workingHour, item.empType)
        employee.setEmployeeId(item.id)
        employeeList.push(employee)
    }
    renderEmployeeList(employeeList)
}


document.getElementById('btnThem').addEventListener('click', function () {
    document.getElementById('btnThemNV').style.display = "block"
    document.getElementById('header-title').innerHTML = "Log In"

})



function saveLocalStorage() {
    localStorage.setItem("employeeList", JSON.stringify(employeeList))
}

function createNewEmployee() {
    //get data from input form
    let username = document.getElementById('username').value
    let fullName = document.getElementById('fullName').value
    let email = document.getElementById('email').value
    let password = document.getElementById('password').value
    let startDate = document.getElementById('startDate').value
    let basicSalary = document.getElementById('basicSalary').value
    let title = document.getElementById('title').value
    let workingHour = document.getElementById('workingHour').value
    return new Employee(username, fullName, email, password, startDate, basicSalary, title, workingHour)

}

function addEmployee() {
    data = createNewEmployee()

    isValid = true
    //check username
    isValid = checkEmpty(data.username, "usernameError", "userName can't be empty") && checkUserNameFormat(data.username, "usernameError")

    //check full name
    isValid &= checkEmpty(data.fullName, "fullNameError", "fullName can't be empty") && checkCharacter(data.fullName, "fullNameError", "fullName can't contain any digit")

    //check email
    isValid &= checkEmpty(data.email, "emailError", "Email can't be empty") && checkEmailFormat(data.email, "emailError")
    // isValid = 
    //check password
    isValid &= checkEmpty(data.password, "passwordError", "password can't be empty") && checkPassword(data.password, "passwordError")

    // //check startDate
    isValid &= checkEmpty(data.startDate, "dateError", "start date can't be empty") && checkDateFormat(data.startDate, "dateError")
    // //check basic salary
    isValid &= checkEmpty(data.basicSalary, "salaryError", "basic salary can't be empty") && checkSalary(data.basicSalary, "salaryError")
    // //check title
    isValid &= checkEmpty(data.title, "titleError", "the title can't be empty") && checkTitle(data.title, "titleError", "Choose employee title")
    // //check working hour
    isValid &= checkEmpty(data.workingHour, "workingHourError", "the working hours can't be empty") && checkWorkingHour(data.workingHour, "workingHourError")

    if (isValid) {
        employeeList.push(data)
        saveLocalStorage()
        renderEmployeeList(employeeList)
    } else {
        return
    }
}

function deleteEmployeeById(id) {
    for (let i = 0; i < employeeList.length; i++) {
        if (employeeList[i].id == id) {
            var index = searchEmployeeIndex(id, employeeList)
            employeeList.splice(index, 1)
        }
    }
    saveLocalStorage()
    renderEmployeeList(employeeList)
}

function getFormInfo() {
    //get data from input form
    let employeeId = document.getElementById("employeeId").value
    let username = document.getElementById('username').value
    let fullName = document.getElementById('fullName').value
    let email = document.getElementById('email').value
    let password = document.getElementById('password').value
    let startDate = document.getElementById('startDate').value
    let basicSalary = document.getElementById('basicSalary').value
    let title = document.getElementById('title').value
    let workingHour = document.getElementById('workingHour').value
    let employee = new Employee(username, fullName, email, password, startDate, basicSalary, title, workingHour)
    employee.setEmployeeId(employeeId)
    return employee
}

function updateEmployeeInfo() {

    infoUpdate = getFormInfo()
    index = searchEmployeeIndex(infoUpdate.id, employeeList)
    console.log("update function: ", infoUpdate.id);
    employeeList[index] = infoUpdate
    saveLocalStorage()
    renderEmployeeList(employeeList)
    document.getElementById('btnDong').click()
}


function searchEmpRanking() {
    var keyword = document.getElementById("ranking").value
    var rankingList = []
    if (keyword.length !== 0) {
        for (let i = 0; i < employeeList.length; i++) {
            keyword = keyword.toLowerCase()
            comparedName = employeeList[i].getEmpType().toLowerCase()
            if (comparedName.includes(keyword))
                rankingList.push(employeeList[i])
        }
    }
    renderRankingList(rankingList)
}


function showEmployeeInfor(id) {
    let panel = document.getElementById("btnThem")
    panel.click()
    position = searchEmployeeIndex(id, employeeList)
    if (position == -1) return;
    let employee = employeeList[position]
    document.getElementById("employeeId").value = employee.id
    document.getElementById('username').value = employee.username
    document.getElementById('fullName').value = employee.fullName
    document.getElementById('email').value = employee.email
    document.getElementById('password').value = employee.password
    document.getElementById('startDate').value = employee.startDate
    document.getElementById('basicSalary').value = employee.basicSalary
    document.getElementById('title').value = employee.title
    document.getElementById('workingHour').value = employee.workingHour
    document.getElementById('btnThemNV').style.display = "none"
    document.getElementById('header-title').innerHTML = "User Information"
}



