


function renderEmployeeList(employeeList) {
    let contentHTML = ""
    for (let i = 0; i < employeeList.length; i++) {
        contentHTML +=
            `   
        <tr>
            <td>${employeeList[i].username}</td>
            <td>${employeeList[i].fullName}</td>
            <td>${employeeList[i].email}</td>
            <td>${employeeList[i].startDate}</td>
            <td>${employeeList[i].title}</td>
            <td>${employeeList[i].getGrossSalary()}</td>
            <td>${employeeList[i].getEmpType()}</td>
            <td style="width: 150px">
                <button class="btn btn-success mr-2" onclick="showEmployeeInfor('${employeeList[i].id}')">
                    <i class="fa-solid fa-pen-to-square"></i>
                </button>
                <button class="btn btn-danger" onclick="deleteEmployeeById('${employeeList[i].id}')">
                    <i class="fa-solid fa-trash"></i>
                </button>
            </td>
            
        </tr>
    `
    }
    document.getElementById('tableDanhSach').innerHTML = contentHTML
}

function renderRankingList(rankingList) {
    let contentHTML =
        `<div class="panel mx-auto mt-5" style="border-bottom: 3px solid black; border-top: 3px solid black; display:block" id="result-panel">
            <table class="table table-bordered table-hover myTable">
            <thead class="text-primary">
                <tr>
                    <th class="nowrap">
                        <span class="mr-1">Tài khoản</span>
                        <i class="fa fa-arrow-up" id="SapXepTang"></i>
                        <i class="fa fa-arrow-down" id="SapXepGiam"></i>
                    </th>
                    <th>Họ và tên</th>
                    <th>Email</th>
                    <th>Ngày làm</th>
                    <th>Chức vụ</th>
                    <th>Tổng lương</th>
                    <th>Xếp loại</th>
                    <th style="position: relative">
                        <em class="fa fa-cog"></em>
                        <button class = "btn btn-danger" onclick="closebtn('result-panel')" style="position: absolute; top:0px; right: 0px">
                            <i class="fa-solid fa-x"></i>
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody id="tableDanhSach">
                `
    if (rankingList.length == 0) {
        contentHTML +=
            ` <tr>
                <td></td>
                <td></td>
                <td>No Result</td>
                <td></td>
                <td></td>

             </tr>`
    } else {
        for (let i = 0; i < rankingList.length; i++) {
            contentHTML += `
            <tr>
            <td>${rankingList[i].username}</td>
            <td>${rankingList[i].fullName}</td>
            <td>${rankingList[i].email}</td>
            <td>${rankingList[i].startDate}</td>
            <td>${rankingList[i].title}</td>
            <td>${rankingList[i].getGrossSalary()}</td>
            <td>${rankingList[i].getEmpType()}</td>
            <td style="width: 150px">
                <button class="btn btn-success mr-2" onclick="showEmployeeInfor('${rankingList[i].id}')">
                    <i class="fa-solid fa-pen-to-square"></i>
                </button>
                <button class="btn btn-danger" onclick="deleteEmployeeById('${rankingList[i].id}')">
                    <i class="fa-solid fa-trash"></i>
                </button>
            </td>
        </tr>
        `
        }
    }
    contentHTML += `</tbody>
                </table>
            </div>`;
    document.getElementById('searchResult').innerHTML = contentHTML;
}

function closebtn(idElement) {
    document.getElementById(idElement).style.display = 'none'

}

function searchEmployeeIndex(id, employeeList) {
    for (let i = 0; i < employeeList.length; i++) {
        if (employeeList[i].id == id) {
            return i
        }
    }
    return -1
}

function showMessageErr(idErr, message) {
    elementTag = document.getElementById(idErr)
    elementTag.innerHTML = message
    elementTag.style.display = "block"
}

function clearInfo() {
    document.getElementById("usernameError").innerHTML = ''
    document.getElementById("fullNameError").innerHTML = ''
    document.getElementById("emailError").innerHTML = ''
    document.getElementById("passwordError").innerHTML = ''
    document.getElementById("dateError").innerHTML = ''
    document.getElementById("salaryError").innerHTML = ''
    document.getElementById("titleError").innerHTML = ''
    document.getElementById("workingHourError").innerHTML = ''
    document.getElementById('username').value = ''
    document.getElementById('fullName').value = ''
    document.getElementById('email').value = ''
    document.getElementById('password').value = ''
    document.getElementById('startDate').value = ''
    document.getElementById('basicSalary').value = ''
    document.getElementById('title').value = ''
    document.getElementById('workingHour').value = ''
}











